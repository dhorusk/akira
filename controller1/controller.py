import interfaces as controller_template
from itertools import product
from typing import Tuple, List
from numpy import random

# Q-Learning constants
ALPHA = 0.10    # learning rate
GAMMA = 0.99    # discount factor

# Track types
GRASS = 0
NORMAL_TRACK = 1
ICE = 2

# Sensors
TRACK_DISTANCE_LEFT = 0
TRACK_DISTANCE_CENTER = 1
TRACK_DISTANCE_RIGHT = 2
ON_TRACK = 3
CHECKPOINT_DISTANCE = 4
CAR_VELOCITY = 5
ENEMY_DISTANCE = 6
ENEMY_POSITION_ANGLE = 7
ENEMY_DETECTED = 8
CHECKPOINT = 9
INCOMING_TRACK = 10
BOMB_DISTANCE = 11
BOMB_POSITION_ANGLE = 12
BOMB_DETECTED = 13

# Actions
RIGHT = 1
LEFT = 2
ACCELERATE = 3
BREAK = 4
NONE = 5

class State(controller_template.State):
    def __init__(self, sensors: list):
        self.sensors = sensors

    def compute_features(self) -> Tuple:
        """
        This function should take the raw sensor information of the car (see below) and compute useful features for selecting an action
        The car has the following sensors:

        self.sensors contains (in order):
            0 track_distance_left: 1-100
            1 track_distance_center: 1-100
            2 track_distance_right: 1-100
            3 on_track: 0 if off track, 1 if on normal track, 2 if on ice
            4 checkpoint_distance: 0-???
            5 car_velocity: 10-200
            6 enemy_distance: -1 or 0-???
            7 enemy_position_angle: -180 to 180
            8 enemy_detected: 0 or 1
            9 checkpoint: 0 or 1
           10 incoming_track: 1 if normal track, 2 if ice track or 0 if car is off track
           11 bomb_distance = -1 or 0-???
           12 bomb_position_angle = -180 to 180
           13 bomb_detected = 0 or 1
          (see the specification file/manual for more details)
        :return: A Tuple containing the features you defined
        """
        return [ self.sensors[TRACK_DISTANCE_LEFT], self.sensors[TRACK_DISTANCE_CENTER] \
                , self.sensors[TRACK_DISTANCE_RIGHT], self.sensors[CAR_VELOCITY] ]

    def discretize_features(self, features: Tuple) -> Tuple:
        """
        This function should map the (possibly continuous) features (calculated by compute features) and discretize them.
        :param features 
        :return: A tuple containing the discretized features
        """
        track_distance_left, track_distance_center \
        , track_distance_right, car_velocity = features

        left_discrete = 0
        if track_distance_left < 40:
            left_discrete = 1
        if track_distance_left < 70:
            left_discrete = 2

        right_discrete = 0
        if track_distance_right < 40:
            right_discrete = 1
        if track_distance_right < 70:
            right_discrete = 2

        ahead_discrete = 0
        if track_distance_center < 40:
            ahead_discrete = 1
        if track_distance_center < 70:
            ahead_discrete = 2

        car_velocity_discrete = 0
        if car_velocity >= 100:
            car_velocity_discrete = 1

        return [left_discrete, right_discrete, ahead_discrete, car_velocity_discrete]

    @staticmethod
    def discretization_levels() -> Tuple:
        """
        This function should return a vector specifying how many discretization levels to use for each state feature.
        :return: A tuple containing the discretization levels of each feature
        """
        return [3, 3, 3, 2]

    @staticmethod
    def enumerate_all_possible_states() -> List:
        """
        Handy function that generates a list with all possible states of the system.
        :return: List with all possible states
        """
        levels = State.discretization_levels()
        levels_possibilities = [(j for j in range(i)) for i in levels]
        return [i for i in product(*levels_possibilities)]


class QTable(controller_template.QTable):
    def __init__(self, init_val=50, init_table=None):
        """
        This class is used to create/load/store your Q-table. To store values we strongly recommend the use of a Python
        dictionary.
        """
        if init_table is not None:
            self.__qtable = init_table
            return

        self.__qtable = {}

        for s in State.enumerate_all_possible_states():
            state_id = State.get_state_id(s)
            self.__qtable[state_id] = {}

            actions = range(1, 5+1) # valid actions range from 1 to 5 inclusive
            for action_id in actions:
                self.__qtable[state_id][action_id] = init_val

    def get_q_value(self, key: State, action: int) -> float:
        """
        Used to securely access the values within this q-table
        :param key: a State object 
        :param action: an action
        :return: The Q-value associated with the given state/action pair
        """
        state_features = key.compute_features()
        discrete_state = key.discretize_features(state_features)
        state_id = State.get_state_id(discrete_state)

        if state_id not in self.__qtable:
            raise KeyError("Unexpected discrete state: {0}".format(discrete_state))

        if action not in self.__qtable[state_id]:
            raise KeyError("Unexpected action value: {0}".format(action))

        return self.__qtable[state_id][action]

    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        """
        Used to securely set the values within this q-table
        :param key: a State object 
        :param action: an action
        :param new_q_value: the new Q-value to associate with the specified state/action pair
        """
        state_features = key.compute_features()
        discrete_state = key.discretize_features(state_features)
        state_id = State.get_state_id(discrete_state)

        if state_id not in self.__qtable:
            raise KeyError("Unexpected discrete state: {0}".format(discrete_state))

        if action not in self.__qtable[state_id]:
            raise KeyError("Unexpected action value: {0}".format(action))

        self.__qtable[state_id][action] = new_q_value

    @staticmethod
    def load(path: str) -> "QTable":
        """
        This method should load a Q-table from the specified file and return a corresponding QTable object
        :param path: path to file
        :return: a QTable object
        """
        f = open(path, 'r')
        q_table_str = f.read()
        q_table = eval(q_table_str)
        f.close()
        return QTable(init_table=q_table)

    def save(self, path: str, *args) -> None:
        """
        This method must save this QTable to disk in the file file specified by 'path'
        :param path: 
        :param args: Any optional args you may find relevant; beware that they are optional and the function must work
                     properly without them.
        """
        f = open(path, 'w')
        f.write(str(self.__qtable))
        f.close()


class Controller(controller_template.Controller):
    def __init__(self, q_table_path: str):
        if q_table_path is None:
            self.q_table = QTable()
        else:
            self.q_table = QTable.load(q_table_path)

    def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_race: bool) -> None:
        """
        This method is called by the learn() method in simulator.Simulation() to update your Q-table after each action is taken
        :param new_state: The state the car just entered
        :param old_state: The state the car just left
        :param action: the action the car performed to get to new_state
        :param reward: the reward the car received for getting to new_state  
        :param end_of_race: boolean indicating if a race timeout was reached
        """
        # TODO: figure out how to use end_of_race
        old_state_q = self.q_table.get_q_value(old_state, action)
        actions = range(1, 5+1) # valid actions range from 1 to 5 inclusive
        new_state_qs = [self.q_table.get_q_value(new_state, a) for a in actions]
        new_state_max_q = max(new_state_qs)
        new_q = old_state_q + ALPHA * (reward + GAMMA * new_state_max_q - old_state_q)
        self.q_table.set_q_value(old_state, action, new_q)

    def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int,
                       end_of_race: bool) -> float:
        """
        This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to the agent
        :param new_state: The state the car just entered
        :param old_state: The state the car just left
        :param action: the action the car performed to get in new_state
        :param n_steps: number of steps the car has taken so far in the current race
        :param end_of_race: boolean indicating if a race timeout was reached
        :return: The reward to be given to the agent
        """
        # TODO: figure out end_of_race
        reward = 0

        if old_state.sensors[CHECKPOINT] or new_state.sensors[CHECKPOINT]:
            reward += 1000

        if new_state.sensors[ON_TRACK] == NORMAL_TRACK \
            and new_state.sensors[INCOMING_TRACK] == NORMAL_TRACK \
            and new_state.sensors[CHECKPOINT_DISTANCE] - old_state.sensors[CHECKPOINT_DISTANCE] < 0 \
            and new_state.sensors[TRACK_DISTANCE_LEFT] < 100 \
            and new_state.sensors[TRACK_DISTANCE_RIGHT] < 100 \
            and new_state.sensors[TRACK_DISTANCE_CENTER] == 100 \
            and action == ACCELERATE:
            reward += 1000

        if new_state.sensors[ON_TRACK] == NORMAL_TRACK \
            and new_state.sensors[TRACK_DISTANCE_CENTER] == 100 \
            and action == ACCELERATE:
            reward += 500

        if new_state.sensors[ON_TRACK] == NORMAL_TRACK \
            and new_state.sensors[CAR_VELOCITY] > 100 \
            and new_state.sensors[CHECKPOINT_DISTANCE] - old_state.sensors[CHECKPOINT_DISTANCE] < 0:
            reward += 300

        if new_state.sensors[ON_TRACK] == NORMAL_TRACK \
            and new_state.sensors[CHECKPOINT_DISTANCE] - old_state.sensors[CHECKPOINT_DISTANCE] < 0:
            reward += 100

        if new_state.sensors[ON_TRACK] == ICE \
            and new_state.sensors[CHECKPOINT_DISTANCE] - old_state.sensors[CHECKPOINT_DISTANCE] < 0 \
            and action != ACCELERATE \
            and action != BREAK \
            and action != NONE:
            reward += 75

        if new_state.sensors[ON_TRACK] == GRASS \
            and new_state.sensors[CHECKPOINT_DISTANCE] - old_state.sensors[CHECKPOINT_DISTANCE] < 0:
            reward += 30

        return reward

    def take_action(self, new_state: State, episode_number: int) -> int:
        """
        Decides which action the car must execute based on its Q-Table and on its exploration policy
        :param new_state: The current state of the car 
        :param episode_number: current episode/race during the training period
        :return: The action the car chooses to execute
        """
        actions = range(1, 5+1) # valid actions range from 1 to 5 inclusive

        # explore a lot in the first 100 episodes
        epsilon = episode_number / 100.0
        if epsilon < 0.50:
            epsilon = 0.50  
        if epsilon > 0.99:
            epsilon = 0.99

        if random.ranf() < (1 - epsilon): # (1-EPSILON) % chance to choose a random action
            return random.choice(actions)

        q_options = [self.q_table.get_q_value(new_state, i) for i in actions]
        best_action, _ = max(enumerate(q_options, 1), key=lambda a: a[1])
        return best_action
